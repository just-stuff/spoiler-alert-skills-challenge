const expect = require('chai').expect;

const getWeightOfExpiredFoodStuff = require('./getWeightOfExpiredFoodStuff');

const contents = [
  { foodType: "produce", smellScore: 0, weightInPounds: 2.2 },
  { foodType: "dairy", smellScore: 4, weightInPounds: 1.4 },
  { foodType: "produce", smellScore: 8, weightInPounds: 1.5 },
  { foodType: "meat", smellScore: 6, weightInPounds: .75 },
  { foodType: "snacks", smellScore: 9, weightInPounds: .55 },
  { foodType: "frozen", smellScore: 10, weightInPounds: 4.8 },
  { foodType: "produce", smellScore: 10, weightInPounds: .8 }
];

const result = getWeightOfExpiredFoodStuff(contents);
// output: {"produce":2.3,"meat":0.75,"frozen":4.8}
describe('getWeightOfExpiredFoodStuff', () => {
  it('value returned to be an object', () => {
    expect(result).to.be.an('object');
  });
  it('object returned contains three keys', () => {
    expect(Object.keys(result).length).to.equal(3);
  });
  it('object returned contains the correct keys', () => {
    expect(result).to.have.all.keys('meat', 'produce', 'frozen');
  });
  it('value of produce is 2.3', () => {
    expect(result['produce']).to.equal(2.3);
  });
  it('value of meat is .75', () => {
    expect(result['meat']).to.equal(.75);
  });
  it('value of frozen is 4.8', () => {
    expect(result['frozen']).to.equal(4.8);
  });
});
