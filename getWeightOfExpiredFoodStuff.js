const getWeightOfExpiredFoodStuff = (inventory) => {
  // validate input
  if (!Array.isArray(inventory)) {
    throw new Error('Invalid input:', inventory);
  }
  if (inventory.length > 0) {
    const item = inventory[0];
    if (!(item.hasOwnProperty('foodType') && item.hasOwnProperty('weightInPounds') && item.hasOwnProperty('smellScore'))) {
      console.log(JSON.stringify(item,undefined,2));
      throw new Error('Invalid input:'+ item);
    }
  }

  // conf standard and non-standard expiration smell scores
  const defaultExpiredSmell = 10;
  const expirationByType = { meat: 6, dairy: 5, produce: 8 };

  const expiredFood = inventory.reduce((acc, item) => {
    // if expired, return new acc with foodType overwritten with new value
    if (item.smellScore >= (expirationByType[item.foodType] ? expirationByType[item.foodType] : defaultExpiredSmell)) {
      return { ...acc , [item.foodType]: acc[item.foodType] ? acc[item.foodType] + item.weightInPounds : item.weightInPounds };
    }

    // if not expired, return acc
    return acc;
  }, {}); // initialize acc to empty object

  return expiredFood;
};

// exporting my personal favorite.  All return the same value
module.exports = getWeightOfExpiredFoodStuff;
