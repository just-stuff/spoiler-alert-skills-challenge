const _ = require('lodash');
const getWeightOfExpiredFoodStuff = require('./getWeightOfExpiredFoodStuff');

const contents = [
  { foodType: "produce", smellScore: 0, weightInPounds: 2.2 },
  { foodType: "dairy", smellScore: 4, weightInPounds: 1.4 },
  { foodType: "produce", smellScore: 8, weightInPounds: 1.5 },
  { foodType: "meat", smellScore: 6, weightInPounds: .75 },
  { foodType: "snacks", smellScore: 9, weightInPounds: .55 },
  { foodType: "frozen", smellScore: 10, weightInPounds: 4.8 },
  { foodType: "produce", smellScore: 10, weightInPounds: .8 }
];

const spoilageWeights = getWeightOfExpiredFoodStuff(contents);
for (foodType in spoilageWeights) {
  console.log(_.upperFirst(foodType)+`: ${spoilageWeights[foodType]}`);
}
